from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from courses.models import Course


class CoursesList(ListView):
	model = Course
	ordering = ['start_date']


class CourseView(DetailView):
	model = Course


class CourseCreate(CreateView):
	model = Course
	fields = ['name', 'start_date']
	success_url = reverse_lazy('courses_list')


class CourseUpdate(UpdateView):
	model = Course
	fields = ['name', 'start_date']
	success_url = reverse_lazy('courses_list')


class CourseDelete(DeleteView):
	model = Course
	success_url = reverse_lazy('courses_list')
