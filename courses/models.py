from django.db import models
from django.utils.text import slugify


class Course(models.Model):
	name = models.CharField(max_length=200, null=False, blank=False, unique=True)
	start_date = models.DateField("Start Date (mm/dd/yyyy)")
	slug = models.SlugField(null=False, blank=False)

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		self.slug = slugify(self.name)
		super().save(*args, **kwargs)
