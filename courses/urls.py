from django.urls import path

from courses.views import CoursesList, CourseView, CourseCreate, CourseUpdate, CourseDelete

urlpatterns = [
	path('', CoursesList.as_view(), name='courses_list'),
	path('view/<slug:slug>', CourseView.as_view(), name='course_view'),
	path('new', CourseCreate.as_view(), name='course_new'),
	path('edit/<slug:slug>', CourseUpdate.as_view(), name='course_edit'),
	path('delete/<slug:slug>', CourseDelete.as_view(), name='course_delete'),
]
